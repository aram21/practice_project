function valid(){
	var val = true;
	var form = document.getElementById('form1');
	var password = form.password.value;
	var RePassword = form.RePassword.value;

	if(password.length < 6) {
			document.getElementById('passVal').innerHTML = "Please enter at least 6 characters.";
			val = false;
	} else {
			document.getElementById("passVal").style.color = "green";
			document.getElementById('passVal').innerHTML = "Done";
			if(password != RePassword) {
			 		document.getElementById('RePassVal').innerHTML = "Passwords do not match.";
			 		val = false;
			 } else if(password == RePassword) {
			 		document.getElementById("RePassVal").style.color = "green";
			 		document.getElementById('RePassVal').innerHTML = "Done";
	 }
	}

	return val;

}
