function valid() {
	var val = true;
    var form = document.getElementById('form');
	var name = form.name.value;
	var surname = form.surname.value;
	var login = form.login.value;
	var email = form.email.value;
	var phone = form.phone.value;
	var password = form.password.value;
	var RePassword = form.RePassword.value;
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneno = /^0(41|43|44|55|91|93|94|95|96|97|98|99)[0-9]{6}$/;

    if(name == 0) {
			document.getElementById('nameVal').innerHTML = "Please enter your name.";
			val = false;
	} else {
			document.getElementById("nameVal").style.color = "green";
			document.getElementById('nameVal').innerHTML = "Done";
	  }

    if(surname == 0) {
			document.getElementById('surnameVal').innerHTML = "Please enter your surname.";
			val = false;
	} else {
			document.getElementById("surnameVal").style.color = "green";
			document.getElementById('surnameVal').innerHTML = "Done";
	  }

	if(login.length < 5) {
			document.getElementById('loginVal').innerHTML = "Please enter at least 5 characters.";
			val = false;
	} else {
			document.getElementById("loginVal").style.color = "green";
			document.getElementById('loginVal').innerHTML = "Done";
	  }

	if(!re.test(email)) {
			document.getElementById('emailVal').innerHTML = "Please enter a valid e-mail address.";
			val = false;
	} else {
			document.getElementById("emailVal").style.color = "green";
			document.getElementById('emailVal').innerHTML = "Done";
      }

    if(!phoneno.test(phone)) {
			document.getElementById('phoneVal').innerHTML = "Please enter a valid phone.";
			val = false;
	} else {
			document.getElementById("phoneVal").style.color = "green";
			document.getElementById('phoneVal').innerHTML = "Done";
	}

	if(password.length < 6) {
			document.getElementById('passVal').innerHTML = "Please enter at least 6 characters.";
			val = false;
	} else {
			document.getElementById("passVal").style.color = "green";
			document.getElementById('passVal').innerHTML = "Done";
			if(password != RePassword) {
			 		document.getElementById('RePassVal').innerHTML = "Passwords do not match.";
			 		val = false;
			 } else if(password == RePassword) {
			 		document.getElementById("RePassVal").style.color = "green";
			 		document.getElementById('RePassVal').innerHTML = "Done";
	 }
	}

	return val;

}

$("#email").focusout(function() {
	var email = $(this).val()
	$.ajax({
          type:'POST',
          url:'forajax.php',
          data: {email: email},
          success:function(msg){

            if(msg == 'invalid Email Filter'){
            	$('#emailVal').html('Please enter a valid e-mail address.');
            	$('#butsub').prop('disabled', true);
            }else if(msg == 'valid Email'){
	            $('#emailVal').html('Этот Email можно использовать.');
	            // email = value;
            }else if(msg == 'invalid Email'){
           		$('#emailVal').html('Этот Email уже занят.');
 	      		$('#butsub').prop('disabled', true);
            }

          }
        });
});

$("#phone").focusout(function() {
	var phone = $(this).val()
	$.ajax({
          type:'POST',
          url:'forajax.php',
          data: {phone: phone},
          success:function(msg){

            if(msg == 'valid Phone'){
            	$('#phoneVal').html('Этот Phone можно использовать.');
            }else if(msg == 'invalid Phone'){
           		$('#phoneVal').html('Этот Phone уже занят.');
           		$('#butsub').prop('disabled', true);
          	}

          }
        });
});

$("#login").focusout(function() {
	var login = $(this).val()
	$.ajax({
          type:'POST',
          url:'forajax.php',
          data: {login: login},
          success:function(msg){

            if(msg == 'valid Login'){
            	$('#loginVal').html('Этот Login можно использовать.');
            }else if(msg == 'invalid Login'){
           		$('#loginVal').html('Этот Login уже занят.');
           		$('#butsub').prop('disabled', true);
          	}

          }
        });
});
